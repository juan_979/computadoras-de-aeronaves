import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Pantalla6 {

	private JFrame frame;
	private JTextField textField;
	private JLabel lblNewLabel_1;

	
	
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Pantalla6 window = new Pantalla6();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	
	
	
	public Pantalla6() {
		initialize();
	}

	
	
	
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(255, 153, 51));
		frame.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Ingresar puesto");
		lblNewLabel.setFont(new Font("Arial Black", Font.PLAIN, 15));
		lblNewLabel.setBounds(69, 70, 146, 19);
		frame.getContentPane().add(lblNewLabel);
		
		textField = new JTextField();
		textField.setBounds(225, 67, 130, 26);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		JButton btnNewButton = new JButton("CALCULAR");
		btnNewButton.setFont(new Font("Arial Black", Font.PLAIN, 11));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int puesto = Integer.parseInt(textField.getText());
				
				if (puesto == 1)
					lblNewLabel_1.setText("Ha ganado la medalla de oro");
				else if (puesto == 2)
					lblNewLabel_1.setText("Ha ganado la medalla de plata");
				else if (puesto == 3)
					lblNewLabel_1.setText("Ha ganado la medalla de bronce");
				else 
					lblNewLabel_1.setText("Siga participando");
				
			}
		});
		btnNewButton.setBounds(235, 104, 117, 29);
		frame.getContentPane().add(btnNewButton);
		
		lblNewLabel_1 = new JLabel("Premio");
		lblNewLabel_1.setFont(new Font("Lucida Grande", Font.BOLD, 15));
		lblNewLabel_1.setBackground(new Color(153, 204, 255));
		lblNewLabel_1.setOpaque(true);
		lblNewLabel_1.setBounds(69, 180, 299, 26);
		frame.getContentPane().add(lblNewLabel_1);
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
}
