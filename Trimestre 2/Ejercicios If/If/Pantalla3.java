

import java.awt.EventQueue;

import javax.swing.JFrame;

import java.awt.Color;

import javax.swing.JLabel;

import java.awt.Font;

import javax.swing.JTextField;

import javax.swing.JButton;

import java.awt.event.ActionListener;

import java.awt.event.ActionEvent;

public class Pantalla3 {

	private JFrame frame;
	
	private JTextField txt;
	
	private JLabel lblResultado;

	
	

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Pantalla3 window = new Pantalla3();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}


	public Pantalla3() {
		initialize();
	}

	
	
	
	private void initialize() {
		frame = new JFrame();
		
		frame.getContentPane().setBackground(new Color(238, 130, 238));
		
		frame.setBounds(100, 100, 450, 300);
		
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		frame.getContentPane().setLayout(null);
		
		JLabel lblNumero = new JLabel("Numero");
		lblNumero.setFont(new Font("Helvetica", Font.BOLD, 20));
		lblNumero.setForeground(new Color(0, 0, 0));
		lblNumero.setBounds(40, 64, 95, 25);
		frame.getContentPane().add(lblNumero);
		
		txt = new JTextField();
		txt.setBounds(219, 69, 116, 22);
		frame.getContentPane().add(txt);
		txt.setColumns(10);
		
		JButton btnCalcular = new JButton("Calcular");
		btnCalcular.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				int numero = Integer.parseInt(txt.getText());
				
				if (numero%2==0)
					lblResultado.setText("Es par");
				else lblResultado.setText("Es impar");
				
			}
		});
		btnCalcular.setFont(new Font("Helvetica", Font.BOLD, 16));
		btnCalcular.setBounds(40, 103, 112, 31);
		frame.getContentPane().add(btnCalcular);
		
		lblResultado = new JLabel("Resultado");
		
		lblResultado.setOpaque(true);
		
		lblResultado.setBackground(new Color(127, 255, 0));
		
		lblResultado.setFont(new Font("Helvetica", Font.BOLD, 20));
		
		lblResultado.setForeground(new Color(0, 0, 0));
		
		lblResultado.setBounds(229, 102, 95, 31);
		frame.getContentPane().add(lblResultado);
	}

}
