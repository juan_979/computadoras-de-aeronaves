import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.Font;
import javax.swing.SwingConstants;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Pantalla9 {

	
	private JFrame frame;
	
	private JTextField txt1;
	
	private JTextField txt2;
	
	private JLabel lblNewLabel_3;

	
	
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Pantalla9 window = new Pantalla9();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	
	
	public Pantalla9() {
		initialize();
	}

	
	
	private void initialize() {
		frame = new JFrame();
		
		frame.getContentPane().setFont(new Font("Helvetica", Font.BOLD, 13));
		
		frame.getContentPane().setBackground(new Color(255, 255, 102));
		
		frame.setBounds(100, 100, 450, 300);
		
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		frame.getContentPane().setLayout(null);
		
		
		
		JLabel lblNewLabel = new JLabel("Jugador 1");
		lblNewLabel.setFont(new Font("Arial Black", Font.PLAIN, 14));
		lblNewLabel.setBounds(27, 51, 74, 16);
		frame.getContentPane().add(lblNewLabel);
		
		
		
		txt1 = new JTextField();
		txt1.setBounds(111, 51, 50, 21);
		frame.getContentPane().add(txt1);
		txt1.setColumns(10);
		
		
		
		JLabel lblNewLabel_1 = new JLabel("Jugador 2");
		lblNewLabel_1.setFont(new Font("Arial Black", Font.PLAIN, 14));
		lblNewLabel_1.setBounds(257, 51, 82, 16);
		frame.getContentPane().add(lblNewLabel_1);
		
		
		
		txt2 = new JTextField();
		txt2.setBounds(340, 51, 50, 21);
		frame.getContentPane().add(txt2);
		txt2.setColumns(10);
		
		
		
		JLabel lblNewLabel_2 = new JLabel("vs");
		lblNewLabel_2.setFont(new Font("Arial Black", Font.PLAIN, 11));
		lblNewLabel_2.setBounds(204, 51, 24, 16);
		frame.getContentPane().add(lblNewLabel_2);
		
		
		
		JButton btnNewButton = new JButton("Calcular");
		btnNewButton.setFont(new Font("Arial Black", Font.PLAIN, 11));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				
				int jugador1 = Integer.parseInt(txt1.getText());
				int jugador2 = Integer.parseInt(txt2.getText());
				
				
				
				if (jugador1 == 0)
					if  (jugador2 == 2) 
				       lblNewLabel_3.setText("Jugador 1 gana");
				
				
				if (jugador1 == 1) 
					if (jugador2 == 0 ) 
				       lblNewLabel_3.setText("Jugador 1 gana");
				
				
				if (jugador1 == 2)
					if(jugador2 == 1) 
							lblNewLabel_3.setText("Jugador 1 gana");
				
				
				if (jugador1 == jugador2)
								lblNewLabel_3.setText("Empate");
				
				
				if (jugador2 == 0)
					if  (jugador1 == 2) 
					       lblNewLabel_3.setText("Jugador 2 gana");
					
					if (jugador2 == 1) 
						if (jugador1 == 0 ) 
					       lblNewLabel_3.setText("Jugador 2 gana");
					
					if (jugador2 == 2)
						if(jugador1 == 1) 
								lblNewLabel_3.setText("Jugador 2 gana");
					
			}
		});
		btnNewButton.setBounds(155, 188, 117, 29);
		frame.getContentPane().add(btnNewButton);
		
		lblNewLabel_3 = new JLabel("Ganador");
		lblNewLabel_3.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_3.setBackground(new Color(102, 204, 102));
		lblNewLabel_3.setOpaque(true);
		lblNewLabel_3.setFont(new Font("Helvetica", Font.BOLD, 15));
		lblNewLabel_3.setBounds(96, 151, 243, 26);
		frame.getContentPane().add(lblNewLabel_3);
	}

}
