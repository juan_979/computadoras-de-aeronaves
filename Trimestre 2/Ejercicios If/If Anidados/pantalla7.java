import java.awt.EventQueue;

import javax.swing.JFrame;

import java.awt.Color;

import javax.swing.JLabel;

import java.awt.Font;

import javax.swing.JTextField;

import javax.swing.JButton;

import java.awt.event.ActionListener;

import java.awt.event.ActionEvent;

public class pantalla7 {

	private JFrame frame;
	private JTextField textField;
	private JLabel lblNewLabel_1;

	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					pantalla7 window = new pantallas7();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	
	public pantalla7() {
		initialize();
	}

	
	private void initialize() {
		frame = new JFrame();
		
		frame.getContentPane().setBackground(new Color(204, 102, 255));
		
		frame.setBounds(100, 100, 450, 300);
		
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		frame.getContentPane().setLayout(null);
		
		
		
		JLabel lblNewLabel = new JLabel("Ingresar curso al que pertenece:");
		lblNewLabel.setFont(new Font("Helvetica", Font.BOLD, 15));
		lblNewLabel.setBounds(10, 41, 233, 19);
		frame.getContentPane().add(lblNewLabel);
		
		
		textField = new JTextField();
		textField.setBounds(253, 39, 130, 26);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		
		
		JButton btnNewButton = new JButton("Calcular");
		btnNewButton.setBackground(new Color(255, 204, 255));
		btnNewButton.setFont(new Font("Arial Black", Font.BOLD, 12));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				int curso = Integer.parseInt(textField.getText());
				
				if (curso == 0)
					lblNewLabel_1.setText("Jardin de infantes");
				if (curso >= 1)
					if (curso <= 6)
						lblNewLabel_1.setText("Nivel primario");
					else if (curso >= 7)
						if (curso <= 12)
							lblNewLabel_1.setText("Nivel secundario");
						else 
							lblNewLabel_1.setText("Error");
				
				
			}
		});
		btnNewButton.setBounds(98, 185, 117, 29);
		frame.getContentPane().add(btnNewButton);
		
		
		lblNewLabel_1 = new JLabel("Nivel al que pertenece");
		
		lblNewLabel_1.setBackground(new Color(0, 255, 255));
		
		lblNewLabel_1.setOpaque(true);
		
		lblNewLabel_1.setFont(new Font("Helvetica", Font.BOLD, 15));
		
		lblNewLabel_1.setBounds(98, 148, 239, 26);
		frame.getContentPane().add(lblNewLabel_1);
	}

}
